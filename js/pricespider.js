/**
 * @file
 * Contains Javascript logic for pricespider.
 */

(function ($, Drupal) {
  'use strict';

  /**
   * Attaches the priceSpider behavior to document.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior to initialize priceSpider.
   */

  Drupal.behaviors.priceSpider = {
    attach: function (context, settings) {
      rebindPriceSpider(context);
    }
  };

  /**
   * Excluding initial load, checks context for .ps-widget and
   * executes rebind if PriceSpider is available on window.
   *
   * @param {HTMLDocument|HTMLElement} context
   *   The context argument for Drupal.attachBehaviors()/detachBehaviors().
   */
  function rebindPriceSpider(context) {
    if (context !== document) {
      if (typeof window.PriceSpider !== 'undefined') {
        if ($(context).find('.ps-widget')) {
          window.PriceSpider.rebind();
        }
      }
      else {
        throw new Error('Cannot find PriceSpider scripts on window');
      }
    }
  }
})(jQuery, Drupal);
