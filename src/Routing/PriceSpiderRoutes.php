<?php

namespace Drupal\pricespider\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Class PriceSpiderRoutes.
 */
class PriceSpiderRoutes implements ContainerInjectionInterface {

  /**
   * The Drupal Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new PriceSpiderRoutes object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
    $routes = [];

    // Have a uri for the Where to buy page?
    if ($wtb_uri = $this->configFactory->get('pricespider.settings')->get('wtb.uri')) {
      $routes['pricespider.wtb'] = new Route(
      // Path to attach route to.
        $wtb_uri,
        // Route defaults.
        [
          '_controller' => '\Drupal\pricespider\Controller\WhereToBuyController::content',
          '_title' => 'Where to Buy',
        ],
        // Route requirements.
        [
          '_permission' => 'access content',
        ]
      );
    }

    return $routes;
  }

}
