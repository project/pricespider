<?php

namespace Drupal\pricespider\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The PriceSpider Configuration Form.
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pricespider_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pricespider.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('pricespider.settings');

    $form['metatags'] = [
      '#type' => 'details',
      '#title' => $this->t('Product Spider Meta Tag values.'),
      '#open' => TRUE,
    ];

    $form['metatags']['ps_account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account Number'),
      '#description' => $this->t('Account Number (value placed in the ps-account metatag)'),
      '#default_value' => $config->get('ps.account'),
      '#required' => FALSE,
    ];

    $form['metatags']['ps_config'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Config ID'),
      '#description' => $this->t('Configuration ID (value placed in the ps-config metatag)'),
      '#default_value' => $config->get('ps.config'),
      '#required' => FALSE,
    ];

    $form['metatags']['ps_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#description' => $this->t('Key (value placed in the ps-key metatag). Used for the WTB button.'),
      '#default_value' => $config->get('ps.key'),
      '#required' => TRUE,
    ];

    $form['js'] = [
      '#type' => 'details',
      '#title' => $this->t('Pricespider Javascript File configuration'),
      '#open' => TRUE,
    ];

    $form['js']['ps_js'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CDN Javascript File'),
      '#description' => $this->t('Path to PriceSpider hosted Javascript file. (example: //cdn.pricespider.com/1/lib/ps-widget.js)'),
      '#default_value' => $config->get('ps.js'),
      '#required' => TRUE,
    ];

    $form['js']['ps_js_universal_tracking_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable CDN Javascript Universal Tracking File.'),
      '#description' => $this->t('When enabled, the Universal Tracking File will be included on every non-admin page.'),
      '#default_value' => $config->get('ps.js_ut_enable'),
      '#return_value' => TRUE,
      '#required' => FALSE,
    ];

    $form['js']['ps_js_universal_tracking'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CDN Javascript Universal Tracking File'),
      '#description' => $this->t('PriceSpider Universal Tracking (UT) is a feature within PriceSpider’s Where-To-Buy (WTB) service, which extends its impression and sales tracking capabilities. The UT feature allows WTB to record consumer experiences within a sub-context; this allows UT/WTB users to query and filter traffic and/or sales at a time of their choosing,based on those contexts. Replace {id} with your unique id provided by Pricespider. Caution: Please do not use Google Tag Manager (GTM) or any other third-party loaders to load any Where to Buy tags. Third-party loaders that loadPriceSpider scripts can block the ability to consistently perform universal tracking. (example: //cdn.pricespider.com/1/{id}/ps-utid.js)'),
      '#default_value' => $config->get('ps.js_ut'),
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="ps_js_universal_tracking_enable"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="ps_js_universal_tracking_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['wtb'] = [
      '#type' => 'details',
      '#title' => $this->t('Where to Buy Page'),
      '#open' => TRUE,
    ];

    $form['wtb']['wtb_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Where To Buy page url'),
      '#description' => $this->t('Location of where the Where to Buy page should live. Leave off trailing slashes.'),
      '#default_value' => $config->get('wtb.uri'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Ensure that a CMS user replaces the {id} parameter.
    if (!empty($form_state->getValue('ps_js_universal_tracking'))) {
      if (preg_match('/{id}/i', $form_state->getValue('ps_js_universal_tracking'))) {
        $form_state->setErrorByName('ps_js_universal_tracking', $this->t('{id} must be replaced with an account or brand id provided by Pricespider.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('pricespider.settings')
      ->set('ps.account', $values['ps_account'])
      ->set('ps.config', $values['ps_config'])
      ->set('ps.key', $values['ps_key'])
      ->set('ps.js', $values['ps_js'])
      ->set('ps.js_ut', $values['ps_js_universal_tracking'])
      ->set('ps.js_ut_enable', $values['ps_js_universal_tracking_enable'])
      ->set('wtb.uri', filter_var($values['wtb_uri'], FILTER_SANITIZE_URL))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
