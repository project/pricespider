<?php

namespace Drupal\pricespider\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\pricespider\Service\PriceSpiderService;

/**
 * Class WhereToBuyController.
 *
 * @package Drupal\pricespider\Controller
 */
class WhereToBuyController extends ControllerBase {

  /**
   * PriceSpiderService.
   *
   * @var \Drupal\pricespider\Service\PriceSpiderService
   */
  protected $priceSpiderService;

  /**
   * WhereToBuyController constructor.
   *
   * @param \Drupal\pricespider\Service\PriceSpiderService $priceSpiderService
   *   The PriceSpider Service.
   */
  public function __construct(PriceSpiderService $priceSpiderService) {
    $this->priceSpiderService = $priceSpiderService;
  }

  /**
   * {@inheritdoc}
   */
  public function content() {

    $build = [
      '#theme' => 'pricespider_wtb_page',
      '#attached' => [
        'library' => ['pricespider/pricespider.js'],
        'html_head' => $this->priceSpiderService->getMetaTags([
          'ps-account',
          'ps-config',
          'ps-language',
          'ps-country',
        ]),
      ],
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $priceSpiderService = $container->get('pricespider');
    return new static(
      $priceSpiderService
    );
  }

}
