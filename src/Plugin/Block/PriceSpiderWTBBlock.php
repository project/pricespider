<?php

namespace Drupal\pricespider\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\pricespider\Service\PriceSpiderService;

/**
 * Displays 'Where to Buy' Price spider functionality.
 *
 * @Block(
 *   id = "pricespider_wtb_block",
 *   admin_label = @Translation("Where to Buy"),
 * )
 */
class PriceSpiderWTBBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * PriceSpiderService.
   *
   * @var \Drupal\pricespider\Service\PriceSpiderService
   */
  protected $priceSpiderService;

  /**
   * PriceSpiderWTBBlock constructor.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\pricespider\Service\PriceSpiderService $priceSpiderService
   *   The PriceSpider Service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PriceSpiderService $priceSpiderService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->priceSpiderService = $priceSpiderService;
  }

  /**
   * Instantiates a new instance of this class.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('pricespider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = [
      '#theme' => 'pricespider_wtb_block',
      '#attached' => [
        'library' => ['pricespider/pricespider.js'],
        'html_head' => $this->priceSpiderService->getMetaTags([
          'ps-account',
          'ps-config',
          'ps-language',
          'ps-country',
        ]),
      ],
    ];

    return $block;
  }

}
