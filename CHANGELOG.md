# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project follows [Drupal.org versioning](http://drupal.org/i/2815471).

## [Unreleased]

## [8.x-1.4] - 2019-04-22
Fixes related to code standards, string translations, installed and required
configuration, ps-country and ps-language meta tags, asynchronously load
external Pricespider JS, calling PriceSpider.rebind() during non-Document
Drupal.attachBehaviors().

### Fixed
* [#3018878](https://www.drupal.org/node/3018878) by 
[butlerbryanc](https://www.drupal.org/u/butlerbryanc),
[jasonawant](https://www.drupal.org/u/jasonawant): PriceSpider doesn't rebind
on async renders
* [#3012143](https://www.drupal.org/node/3012143) by
[kamkejj](https://www.drupal.org/u/kamkejj),
[jasonawant](https://www.drupal.org/u/jasonawant): JS Library doesn't properly
define the async attribute
* [#3011881](https://www.drupal.org/node/3011881) by
[kamkejj](https://www.drupal.org/u/kamkejj),
[jasonawant](https://www.drupal.org/u/jasonawant): Code standards updates
* [#3011872](https://www.drupal.org/node/3011872) by
[kamkejj](https://www.drupal.org/u/kamkejj),
[jasonawant](https://www.drupal.org/u/jasonawant): PriceSpiderService getting
country code from language doesn't work
* [#3011858](https://www.drupal.org/node/3011858) by
[kamkejj](https://www.drupal.org/u/kamkejj),
[jasonawant](https://www.drupal.org/u/jasonawant): AdminSettingsForm fields not
required, default values and code styling
* [#3024782](https://www.drupal.org/node/3024782) by
[kkohlbrenner](https://www.drupal.org/u/kkohlbrenner),
[jasonawant](https://www.drupal.org/u/jasonawant),
[cosmicdreams](https://www.drupal.org/u/cosmicdreams): Install file incorrectly
named
* [#2895595](https://www.drupal.org/node/2895595) by
[jayesh_makwana](https://www.drupal.org/u/jayesh_makwana),
[kamkejj](https://www.drupal.org/u/kamkejj),
[jasonawant](https://www.drupal.org/u/jasonawant): $this->t() should be used
instead of t() for Drupal 8 version


## [8.x-1.5] - 2019-12-10
Adds support for PriceSpider Universal Tracking script usage and removes unused
code.

### Feature
* [#3084681](https://www.drupal.org/node/3084681) by
[kkohlbrenner](https://www.drupal.org/u/kkohlbrenner),
[jasonawant](https://www.drupal.org/u/jasonawant): Add optional configuration
to attach Universal Tracking script to pages

### Task
* [#3099842](https://www.drupal.org/node/3099842) by
[jasonawant](https://www.drupal.org/u/jasonawant),
[kkohlbrenner](https://www.drupal.org/u/kkohlbrenner): Removed unused
pricespider_entity_bundle_rename()

## [8.x-1.6] - 2022-01-20
Adds new permission for administering settings form. Drupal 9 compatability fixes.

### Feature
* [#3161173](https://www.drupal.org/project/pricespider/issues/3161173) by
[milindk](https://www.drupal.org/u/milindk): Create Administer PriceSpider
Configuration permission to govern settings form

### Task
[#3148482](https://www.drupal.org/project/pricespider/issues/3148482) by
[Project-Update-Bot](https://www.drupal.org/u/project-update-bot)
