CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module provides a basic integration with the PriceSpider service. Currently
this module only provides support for the page-embedded Where-to-Buy (WTB)
experience, as well as PriceSpider's Universal Tracking functionality.

  * For a full description of the module, visit the project page:
    https://www.drupal.org/project/pricespider

  * To submit bug reports and feature suggestions, or track changes:
    https://www.drupal.org/project/issues/pricespider

REQUIREMENTS
------------

This module does not require other contributed modules. It does require a
PriceSpider account.

RECOMMENDED MODULES
-------------------

There are no other recommended modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * Configure the user permissions in Administration » People » Permissions:

   - Grant "administer site configuration" permission

     To configure the PriceSpider module settings, users need a use role with
     "administer site configuration" permission. @todo: update this section when
     https://www.drupal.org/i/3103907 is complete.

 * Configure PriceSpider Product Where To Buy functionality

   - Navigate to configuration page at path /admin/config/services/pricespider

   - Add your PriceSpider account ps-key configuration ID

   - Save configuration

   - Navigate to Drupal's Regional Settings at path
     /admin/config/regional/settings

   - Configure the site's Default country if it's not already

   - Save configuration

   - Navigate to the administration edit page for the entity bundle path, e.g. a
     product content type at path /admin/structure/types/manage/product

   - Click the PriceSpider vertical tab

   - Mark the "Use as a PriceSpider product" checkbox

   - Select which field will be used for the SKU value

   - Save entity bundle configuration

   - Navigate to the entity bundle display configuration path, e.g.
     /admin/structure/types/manage/product/display

   - Ensure the "PriceSpider Where to Buy button" pseudo field is configured to
     display, not disabled

 * Configure the PriceSpider Where to Buy page functionality

   - Navigate to configuration page at path /admin/config/services/pricespider

   - Add and save the "Where To Buy page url" value

   - Save configuration

 * Configure the PriceSpider Universal Tracking functionality

   - Navigate to configuration page at path /admin/config/services/pricespider

   - Mark the "Enable CDN Javascript Universal Tracking File" checkbox

   - Add the "CDN Javascript Universal Tracking File" value

   - Save configuration

TROUBLESHOOTING
---------------

 * If you are using custom twig templates to render product content and the
   PriceSpider Where to Buy button is not rendering, ensure the content render
   array is printed within the template, or specifically the PriceSpider Where
   to Buy button content.pricespider_wtb_button is printed.

FAQ
---
There are no FAQs at this time.

MAINTAINERS
-----------

Current maintainers:
 * Michael Miles (mikemiles86) - https://www.drupal.org/u/mikemiles86
 * Jason Want (jasonawant) - https://www.drupal.org/u/jasonawant
